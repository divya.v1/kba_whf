Prerequisites:

1.Install VScode: 

 https://code.visualstudio.com/docs/?dv=linux64_deb  

Installation command in terminal

sudo dpkg -i code_1.76.0-1677667493_amd64.deb

2.Install curl

sudo apt-get update
 
sudo apt install curl -y

3.Docker – Version 19.03 or above

Download the docker script using the command and provide execute permission.

curl -fsSL https://get.docker.com -o get-docker.sh

chmod +x get-docker.sh

Execute the script using the command

./get-docker.sh

Remove the docker script
rm get-docker.sh

sudo usermod -aG docker $USER

Now, reboot your machine


Check the installed docker version using the command

docker version


4. Docker-compose – version 1.2.5

Install docker-compose using apt

sudo apt install docker-compose

Check the docker-compose version using the command

docker-compose version

5 .Build Essentials

Install build-essentials using the apt

sudo apt install build-essential

6 Nodejs – 16.x

Get the node install script using curl

curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash -


Install Node.js using apt

sudo apt install -y nodejs

Check the nodeJS version

node -v


Check npm version
npm -v



7. IBM extension: ibm - blockchain platform-2.0.8.vsix (download) using below link

https://gitlab.com/CHF_KBA/kba_chf_ibmblockchainplatformextension_vscode/-/raw/main/ibm-blockchain-platform-2.0.8.vsix?inline=false 


Then from VScode extensions>  select (... more option) > Install from VSIX , from there select the downloaded location of IBM BC extension.


8. Preparing the Network(Setting the configuration of the network)

In our example we are considering two organizations: Buyer and Seller.

export MICROFAB_CONFIG='{
"port": 7070,
"endorsing_organizations":[
{
"name": "Producers"
},
{
"name": "Sellers"
}
],
"channels":[
{
"name": "mango-channel",
"endorsing_organizations":[
"Producers",
"Sellers"
]
}
]
}'

After you are done with exporting configuration, here comes the final command:
$ docker run -e MICROFAB_CONFIG -p 8080:8080 ibmcom/ibp-microfab

9. Integrating IBM BC with the Fabric network
 
Fabric environment
Add Local or remote environment
	Add microfab network (select the default entries since we bootstrapped on the same network.


Enter a name
MangoTrade



It will automatically detect Hyperledger Fabric  n/w running locally.

[It automatically adds wallet, Gateway]




10 . Scaffolding Project[CC development, deployment etc)

10.1.Go to IBM Blockchain extension and select the more options button (…) from the Smart Contracts window.

10.2.Choose the “Create New Project” menu for generating a new smart contract project.

10.3. Choose Contract Type
Now you can see a new prompt asking for choosing the contract type. Let’s pick the “Default Contract” and proceed.

10.4. Choose language: javascript

10.5.Name the Asset: Mango

10.6. Browse Location
 	10.7.Create Folder(KBA-Mango-Contract)
10.8.Open in Current Window

[ If Contract is already in a folder, open the folder and  just add the environment that will automatically add the wallet and gateway]
  


11.  Connect with gateway
Two organizations WILL be there as per configuration, select one and connect to corresponding identity.


Tear Down the Network:     
Disconnect environment
Delete environment
In terminal ctrl + c





